package com.company.graph2;

import java.util.ArrayList;
import java.util.HashMap;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        HashMap<Integer, Integer> map = new HashMap<>();

        int INF = Integer.MAX_VALUE / 2;
        for (int i=0; i<adjacencyMatrix.length; i++){
            map.put(i, INF);
        }
        map.put(startIndex, 0);
        boolean[] done = new boolean[adjacencyMatrix.length];
        for (int i=0; i<adjacencyMatrix.length; i++){
            done[i]=false;
        }
        int index;
        while (true){
            index=-1;
            for (int i=0; i<adjacencyMatrix.length; i++) {
                if (!done[i] && map.get(i) < INF && (index==-1 || (map.get(index) >= map.get(i)))) {
                    index = i;
                }
            }
            if (index==-1){break;}

            done[index]=true;
            for (int i=0; i<adjacencyMatrix.length; i++){
                if (!done[i]){
                    if (map.get(index)+adjacencyMatrix[index][i]<map.get(i) && adjacencyMatrix[index][i]!=0){
                        map.put(i, map.get(index)+adjacencyMatrix[index][i]);
                    }
                }
            }
        }
        return map;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        HashMap<Integer, Integer> map = new HashMap();
        int index=0;
        int number;
        int n =  0;
        map.put(0,adjacencyMatrix[0][0]);
        boolean[] done = new boolean[adjacencyMatrix.length];
        done[0]=true;
        int[] key = new int[adjacencyMatrix.length];
        key[0]=0;
        while (map.size()!=adjacencyMatrix.length){
            number = Integer.MAX_VALUE / 2;
            for (int j=0; j<map.size(); j++) {
                for (int i = 0; i < adjacencyMatrix.length; i++) {
                    if (adjacencyMatrix[key[j]][i] != 0 && adjacencyMatrix[key[j]][i] < number && !done[i]) {
                        number = adjacencyMatrix[key[j]][i];
                        n = i;
                    }
                }
            }
            key[map.size()]=n;
            done[n]=true;
            map.put(n, number);
        }
        int answer = 0;
        for (int i=0; i<map.size(); i++){
            answer = answer + map.get(i);
        }
        //System.out.println(map);
        return answer;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        int[] key = new int[adjacencyMatrix.length];
        for (int i=0; i<key.length; i++){
            key[i]=i;
        }
        int number;
        int ans=0;
        //HashMap<Integer, Integer> map = new HashMap();
        int n=-1;
        int m=-1;
        int flag =0;
        while (flag!=adjacencyMatrix.length-1) {
            flag =0;
            number = Integer.MAX_VALUE / 2;
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                for (int j = 0; j < adjacencyMatrix.length; j++) {
                    if (key[i]!=key[j]) {
                        if (adjacencyMatrix[i][j]<number && adjacencyMatrix[i][j] != 0){
                            number = adjacencyMatrix[j][i];
                            n = i;
                            m = j;
                        }
                    }
                }
            }
            ArrayList<Integer> num = new ArrayList<>();
            for (int i=0; i<key.length; i++){
                if (key[i]==key[n] || key[i]==key[m]){
                    num.add(i);
                }
            }

            for (int i=0; i<num.size(); i++) {
                if (n < m) {
                    key[num.get(i)]=n;

                } else {
                    key[num.get(i)]=m;

                }
            }
            ans=ans+number;
            num.clear();


            for (int i=1; i<key.length; i++){
                if (key[i]==key[i-1]){
                    flag++;
                }
            }
        }
        return ans;
    }
}
